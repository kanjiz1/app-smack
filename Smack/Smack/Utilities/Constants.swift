//
//  Constants.swift
//  Smack
//
//  Created by Oforkanji Odekpe on 1/24/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success:Bool)-> ()

//URL Constants
let BASE_URL = "https://getchatty.herokuapp.com/v1"
let URL_REGISTER = "\(BASE_URL)account/register"
let URL_LOGIN = "\(BASE_URL)account/login"

//Segues
let TO_LOGIN = "toLogin"
let TO_CREATE_ACCOUNT = "toCreateAccount"
let UNWIND = "unwindToChannel"

//User Defaults
let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL = "userEmail"

//Headers
let HEADER = [
    "Content-type" : "application/json; charset=utf-8"
]
