//
//  CreateAccountVC.swift
//  Smack
//
//  Created by Oforkanji Odekpe on 1/24/18.
//  Copyright © 2018 Oforkanji Odekpe. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController, UITextViewDelegate {

    //Outlets
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func createAccountPressed(_ sender: Any) {
        guard let email = emailText.text, emailText.text != "" else
        {return}
        
        guard let password = passwordText.text, passwordText.text != "" else
        {return}
        
        AuthService.instance.registerUser(email: email, password: password) { (success) in
            if success{
                print("registered User")
                AuthService.instance.loginUser(email: email, password: password, completion: { (success) in
                    if success{
                        debugPrint("Logged in User", AuthService.instance.authToken)
                    }
                })
            }
        }
    }
    
    @IBAction func pickAvatarPressed(_ sender: Any) {
    }
    
    @IBAction func pickBGColorPressed(_ sender: Any) {
    }
    
    @IBAction func closePressed(_ sender: Any) {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }
}
